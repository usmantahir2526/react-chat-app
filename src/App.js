import { useEffect, useRef, useState } from "react";
import {
  VStack,
  Button,
  Container,
  HStack,
  Box,
  Input,
} from "@chakra-ui/react";
import "./App.css";
import Message from "./components/Message";
import {
  signOut,
  onAuthStateChanged,
  getAuth,
  GoogleAuthProvider,
  signInWithPopup,
} from "firebase/auth";
import { app } from "./firebase/firebase";
import {
  addDoc,
  collection,
  getFirestore,
  serverTimestamp,
  onSnapshot,
  query,
  orderBy,
} from "firebase/firestore";

const auth = getAuth(app);
const db = getFirestore(app);

const loginHandler = () => {
  const provider = new GoogleAuthProvider();
  signInWithPopup(auth, provider);
};

const logoutHandler = () => signOut(auth);

function App() {
  const [users, setUsers] = useState(false);
  const [message, setMessage] = useState("");
  const [messages, setMessages] = useState([]);
  const divForScroll = useRef(null);

  const inputHandler = (e) => {
    setMessage(e.target.value);
  };

  const submitHandler = async (e) => {
    e.preventDefault();
    try {
      await addDoc(collection(db, "Messages"), {
        text: message,
        uid: users.uid,
        uri: users.photoURL,
        createdAt: serverTimestamp(),
      });
      setMessage("");
      divForScroll.current.scrollIntoView({ behavior: "smooth" });
    } catch (error) {
      alert(error);
    }
  };

  useEffect(() => {
    const q = query(collection(db, "Messages"), orderBy("createdAt", "asc"));

    const unsubscribe = onAuthStateChanged(auth, (data) => {
      setUsers(data);
    });

    const unSubscribeForMessages = onSnapshot(q, (snap) => {
      setMessages(
        snap.docs.map((item) => {
          const id = item.id;
          return { id, ...item.data() };
        })
      );
    });

    return () => {
      unsubscribe();
      unSubscribeForMessages();
    };
  }, []);
  return (
    <Box bg={"red.100"}>
      {users ? (
        <Container h={"100vh"} bg={"white"}>
          <VStack h={"full"} paddingY={4}>
            <Button onClick={logoutHandler} colorScheme="red" w={"full"}>
              LOGOUT
            </Button>
            <VStack h={"full"} w={"full"} overflowY={"auto"}>
              {messages.map((item, index) => (
                <Message
                  key={index}
                  uri={item.uri}
                  title={item.text}
                  user={users.uid === item.uid ? "me" : "other"}
                />
              ))}
              <div ref={divForScroll}></div>
            </VStack>
            <form style={{ width: "100%" }} onSubmit={submitHandler}>
              <HStack>
                <Input
                  value={message}
                  onChange={inputHandler}
                  placeholder="Enter A Message..."
                />
                <Button type="submit" colorScheme="red">
                  Send
                </Button>
              </HStack>
            </form>
          </VStack>
        </Container>
      ) : (
        <VStack h="100vh" justifyContent={"center"}>
          <Button onClick={loginHandler}>Sign In With Google</Button>
        </VStack>
      )}
    </Box>
  );
}

export default App;
